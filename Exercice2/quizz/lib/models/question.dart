class Question {
  String questionText;
  String isCorrect;
  String imageUrl = "";

  Question({required this.questionText, required this.isCorrect});

  // Setters
  void setQuestion(String quest) {
    questionText = quest;
  }

  void setAnswer(String answer) {
    this.isCorrect = answer;
  }

  void setImageUrl(String getImageUrl) {

    imageUrl = getImageUrl;
  }

  // Getters
  String getQuestion() {
    return questionText;
  }

  String getAnswer() {
    return this.isCorrect;
  }

  String getImageUrl() {
    return imageUrl;
  }
}
