import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:quizz/models/questionRepository.dart';
import 'package:quizz/models/question.dart';

part 'quizzPageState.dart';

class QuizzpageCubit extends Cubit<QuizzPageState> {
  QuizzpageCubit(this._index, this._questions) : super(QuizzpageInitial(0, getQuestions()));

  List<Question> _questions = [];
  int _index = 0;

  void nextQuestion() {
    index = (index + 1) % questions.length;
    emit(QuizzpageInitial(index, questions));
  }

  int get index => _index;

  set index(int value) {
    _index = value;
    emit(QuizzpageInitial(index, questions));
  }

  List<Question> get questions => _questions;

  set questions(List<Question> value) {
    _questions = value;
    emit(QuizzpageInitial(index, questions));
  }
}