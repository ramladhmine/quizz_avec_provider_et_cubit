part of 'quizzPageCubit.dart';

@immutable
abstract class QuizzPageState {}

class QuizzpageInitial extends QuizzPageState {
  int index = 0;
  List<Question> questions = [];

  QuizzpageInitial(this.index, this.questions);
}