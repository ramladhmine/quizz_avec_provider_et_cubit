﻿import 'package:flutter/material.dart';
import 'package:quizz/views/quizzPage.dart';
import 'package:quizz/models/questionRepository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:quizz/cubit/quizzPageCubit.dart';

void main() {
  runApp(const MyApp());
}
class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
        create: (_) => QuizzpageCubit(0, getQuestions()),
        child: BlocBuilder<QuizzpageCubit, QuizzPageState>(
          builder: (_, theme) {
            return MaterialApp(
              title: 'Questions/Réponses',
              debugShowCheckedModeBanner: false,
              theme: ThemeData(
                primarySwatch: Colors.blueGrey,
              ),
              home: Scaffold(
                backgroundColor: Colors.blueGrey,
                body: SafeArea(
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10.0),
                    child: QuizzPage(title: "Questions/Réponses"),
                  ),
                ),
              ),
            );
          },
        ));

  }
}


