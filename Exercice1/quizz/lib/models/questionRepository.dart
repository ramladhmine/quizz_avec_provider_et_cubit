import 'package:quizz/models/question.dart';

List<Question> getQuestions() {

  List<Question> _questionList = [];

  //Question 1
  Question question1 = new Question(questionText: '', isCorrect:'');
  question1.setQuestion("La Russie est le plus grand pays du monde ");
  question1.setAnswer("True");
  question1.setImageUrl("https://images.pexels.com/photos/8285167/pexels-photo-8285167.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1");
  _questionList.add(question1);

  //Question 2
  Question question2 = new Question(questionText: '', isCorrect: '');
  question2.setQuestion("La phrase suivante est bien orthographiée : 'Elle s'est lavé les cheveux ' ");
  question2.setAnswer("True");
  question2.setImageUrl("https://images.pexels.com/photos/7852674/pexels-photo-7852674.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1");
  _questionList.add(question2);

  //Question 3
  Question question3 = new Question(questionText: '', isCorrect: '');
  question3.setQuestion("Sur la photo, vous pouvez voir un éléphant d'Afrique ");
  question3.setAnswer("True");
  question3.setImageUrl("https://images.pexels.com/photos/1057366/pexels-photo-1057366.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1");
  _questionList.add(question3);

  //Question 4
  Question question4 = new Question(questionText: '', isCorrect: '');
  question4.setQuestion("La capitale de l'Islande est Dublin ");
  question4.setAnswer("False");
  question4.setImageUrl("https://images.pexels.com/photos/870711/pexels-photo-870711.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1");
  _questionList.add(question4);

  //Question 5
  Question question5 = new Question(questionText: '', isCorrect: '');
  question5.setQuestion("Il y a neuf planètes dans le système solaire ");
  question5.setAnswer("False");
  question5.setImageUrl("https://images.pexels.com/photos/13373980/pexels-photo-13373980.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1");
  _questionList.add(question5);

  //Question 6
  Question question6 = new Question(questionText: '', isCorrect: '');
  question6.setQuestion("Le jus de raisin est plus sucré que le Coca-Cola ");
  question6.setAnswer("True");
  question6.setImageUrl("https://images.pexels.com/photos/708777/pexels-photo-708777.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1");
  _questionList.add(question6);

  //Question 7
  Question question7 = new Question(questionText: '', isCorrect: '');
  question7.setQuestion("La Chine est le premier producteur d'or au monde ");
  question7.setAnswer("True");
  question7.setImageUrl("https://images.pexels.com/photos/366551/pexels-photo-366551.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1");
  _questionList.add(question7);

  //Question 8
  Question question8 = new Question(questionText: '', isCorrect: '');
  question8.setQuestion("La pastèque contient plus de 90% d'eau ");
  question8.setAnswer("True");
  question8.setImageUrl("https://images.pexels.com/photos/260426/pexels-photo-260426.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1");
  _questionList.add(question8);

  //Question 9
  Question question9 = new Question(questionText: '', isCorrect: '');
  question9.setQuestion("Le nombre de régions en France métropolitaine est de 22 ");
  question9.setAnswer("True");
  question9.setImageUrl("https://images.pexels.com/photos/5781917/pexels-photo-5781917.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1");
  _questionList.add(question9);

  //Question 10
  Question question10 = new Question(questionText: '', isCorrect: '');
  question10.setQuestion("Les zèbres sont des animaux blancs à rayures noires ");
  question10.setAnswer("True");
  question10.setImageUrl("https://images.pexels.com/photos/14896605/pexels-photo-14896605.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1");
  _questionList.add(question10);


  return _questionList;
}
