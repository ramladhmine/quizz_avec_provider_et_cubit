import 'package:flutter/material.dart';
import 'package:quizz/views/quizzPage.dart';
import 'package:provider/provider.dart';
import 'package:quizz/models/questionRepository.dart';
import 'package:quizz/provider/quizzPageProvider.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Questions/Réponses",
      theme: ThemeData(
      ),
      home: ChangeNotifierProvider(
        create: (_) => QuizzPageProvider(0, getQuestions()),
        child: QuizzPage(title: "Questions/Réponses"),
      ),
    );
  }
}