import 'package:flutter/material.dart';
import 'package:quizz/models/question.dart';

class QuizzPageProvider with ChangeNotifier {

  int _index;
  List<Question> _questionList = [];

  QuizzPageProvider(this._index, this._questionList);

  void nextQuestion() {
    index = (index + 1) % questions.length;
    notifyListeners();
  }

  //Getters
  int get index => _index;
  List<Question> get questions => _questionList;

  //Setters
  set index(int value) {
    _index = value;
    notifyListeners();
  }
  set questions(List<Question> value) {
    _questionList = value;
    notifyListeners();
  }
}