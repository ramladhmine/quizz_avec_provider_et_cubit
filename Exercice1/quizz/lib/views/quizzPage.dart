import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:quizz/provider/quizzPageProvider.dart';


class QuizzPage extends StatelessWidget {
  const QuizzPage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  Widget build(BuildContext context) {

    final _quizzpageProvider = Provider.of<QuizzPageProvider>(context);

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blueGrey,
        title: Text("Questions/Réponses"
            ,style: TextStyle(color: Colors.white, fontSize: 25)
          ),
          centerTitle: true,
      ),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 30, vertical: 30),
        width: MediaQuery.of(context).size.width,
        decoration: new BoxDecoration(color: Colors.blueGrey),

        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            CachedNetworkImage(
              imageUrl: _quizzpageProvider.questions[_quizzpageProvider.index]
                  .getImageUrl(),
            ),
            const SizedBox(
              height: 1,
            ),
            Container(
              margin: const EdgeInsets.all(20),
              height: 100,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  border: Border.all(
                    color: Colors.white12,
                  ),
                  borderRadius: const BorderRadius.all(Radius.circular(10))),
              child: Text(
                _quizzpageProvider.questions[_quizzpageProvider.index].getQuestion() + " ?",
                textAlign: TextAlign.center,
                style: const TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                    fontWeight: FontWeight.w500),
              ),
            ),
            const Spacer(),
            Container (
              padding: EdgeInsets.symmetric(vertical: 50),
              child : Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      elevation: 5,
                      primary: Colors.green,
                    ),
                    onPressed: () {
                      _quizzpageProvider.nextQuestion();
                    },
                    child: Text(
                        "VRAI",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 17,
                            fontWeight: FontWeight.w400),
                        textAlign: TextAlign.center,
                      ),
                  )),

                const SizedBox(
                  width: 15,
                ),

                Expanded(
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      elevation: 5,
                      primary: Colors.red,
                    ),
                    onPressed: () {
                      _quizzpageProvider.nextQuestion();
                    },
                    child: Text(
                        "FAUX",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 17,
                            fontWeight: FontWeight.w400),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                const SizedBox(
                  width: 20,
                ),
                Expanded(
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      elevation: 15,
                      primary: Colors.blueGrey,
                    ),
                    onPressed: () {
                      _quizzpageProvider.nextQuestion();
                    },
                    child: const Icon(Icons.arrow_forward),
                  ),
                ),

              ],
            ),),
          ],
        ),
      ),
    );
  }
}